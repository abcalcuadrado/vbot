package com.vbot

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util
import java.util.Date

import akka.actor.{ActorSystem, Props}
import akka.testkit.TestKit
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.regions.Regions
import com.amazonaws.services.ec2.{AmazonEC2Client, AmazonEC2ClientBuilder}
import com.amazonaws.services.ec2.model.{LaunchSpecification, RequestSpotInstancesRequest}
import com.sksamuel.elastic4s.{ElasticClient, ElasticsearchClientUri}
import com.typesafe.config.ConfigFactory
import com.vbot.NodeProtocol.CreateInstance
import com.vbot.TimeManagerProtocol.{Init, Tick}
import org.scalatest.{FunSpec, FunSpecLike}

import collection.JavaConverters._
import scala.language.implicitConversions

/**
  * Created by vbot on 1/13/17.
  */
object TimeManagerTest {

}

class TimeManagerTest extends TestKit(ActorSystem(
  "TestKitUsageSpec")) with FunSpecLike {

  describe("TimeManager") {
    it("should print a log") {
      val format = new SimpleDateFormat("E")
      val actualDay = LocalDate.now().format(DateTimeFormatter.ofPattern("E"))
      val finalDay = LocalDate.now().plusDays(1).format(DateTimeFormatter.ofPattern("E"))

      println(actualDay, finalDay)
    }
    it("should tick every fixed time") {
      system.actorOf(Props[TimeManager]) ! Init()
      Thread.sleep(300000)
    }


    it("create instnace") {
      val a =system.actorOf(Props(classOf[InstanceRegionFactory], Regions.US_EAST_1))
      a! CreateInstance()
      a! CreateInstance()
      Thread.sleep(300000)
    }

    it("ec2 test") {
      val ec2 = AmazonEC2ClientBuilder.standard().withRegion(Regions.US_EAST_1).build()
      val requestRequest = new RequestSpotInstancesRequest()
      requestRequest.setSpotPrice("0.01")
      requestRequest.setInstanceCount(Integer.valueOf(1))
      val launchSpecification = new LaunchSpecification()
      launchSpecification.setImageId("ami-e13739f6")
      launchSpecification.setInstanceType("m3.medium")
      val securityGroups = new util.ArrayList[String]()
      securityGroups.add("vbot-instance-security-group")
      launchSpecification.setSecurityGroups(securityGroups)
      requestRequest.setLaunchSpecification(launchSpecification)
      val requestResult = ec2.requestSpotInstances(requestRequest)
      val requestResponses = requestResult.getSpotInstanceRequests
      requestResponses.asScala.foreach(println)

      //      // Setup an arraylist to collect all of the request ids we want to
      //      // watch hit the running state.
      //      val spotInstanceRequestIds = new ArrayList[String]()
      //      println(requestResult.toString)


    }
    //    it("t"){
    //      NodeActivityLogger.scheduledNode("hoila")
    //    }
    //    it("should print schedules per hour") {
    //      val timeManager = system.actorOf(Props[TimeManager])
    //      timeManager ! Tick()
    //    }

  }

}
