package com.vbot

import java.util.{Calendar, Date, UUID}
import java.util.concurrent.{ThreadLocalRandom, TimeUnit}

import akka.actor.{Actor, ActorIdentity, ActorRef, Identify, Props}
import akka.actor.Actor.Receive
import akka.event.Logging
import com.typesafe.config.ConfigFactory
import com.vbot.NodeProtocol.{CreateInstance, DeleteInstance, Init, SuccessfulCreation}
import com.vbot.TimeManagerProtocol.{SetActiveNode, Tick}
import com.sksamuel.elastic4s.ElasticDsl._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import com.amazonaws.auth.{AWSStaticCredentialsProvider, BasicAWSCredentials}
import com.amazonaws.regions.Regions
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder
import com.amazonaws.services.ec2.model.{CancelSpotInstanceRequestsRequest, TerminateInstancesRequest}
import com.vbot.InstanceRegionFactoryProtocol.CreateNode

import collection.JavaConverters._
import scala.concurrent.duration._
import scala.concurrent.Future

/**
  * Created by vbot on 1/14/17.
  */


object NodeProtocol {

  case class Init()

  case class CreateInstance()

  case class DeleteInstance()

  case class SuccessfulCreation(iId: String, sRIId: String, r: Regions)

}

object Node {
  val MAX_DELAY_MILISECONDS_FOR_CREATION = "maxDelayMilisecondsForCreation"
  val MAX_TTL_MILISECONDS = "maxTTLMiliseconds"
  val MIN_TTL_MILISECONDS = "minTTLMiliseconds"
  val NODE = "node"
  val NODE_ACTIVITY_INDEX = "node_activity"
  val LIFECYCLE_TYPE = "lifecycle"
  val REGIONS = "regions"
  val US_EAST_1 = "us-east-1"
  val US_EAST_2 = "us-east-2"
  val US_WEST_1 = "us-west-1"
  val US_WEST_2 = "us-west-2"
  val CA_CENTRAL_1 = "ca-central-1"
  val EU_CENTRAL_1 = "eu-central-1"
  val EU_WEST_1 = "eu-west-1"
  val EU_WEST_2 = "eu-west-2"
  val SA_EAST_1 = "sa-east-1"
  val AP_SOUTHEAST_2 = "ap-southeast-2"
}

class Node(regionActors: Map[String, ActorRef]) extends Actor {
  implicit val timeout = Timeout(5 seconds)

  val log = Logging(context.system, this)
  val conf = ConfigFactory.load()
  val maxMiliseconds = conf.getInt(s"${Node.NODE}.${Node.MAX_DELAY_MILISECONDS_FOR_CREATION}")
  val maxTTLMinutes = conf.getInt(s"${Node.NODE}.${Node.MAX_TTL_MILISECONDS}")
  val minTTLMinutes = conf.getInt(s"${Node.NODE}.${Node.MIN_TTL_MILISECONDS}")
  val regionsConf = conf.getConfig(Node.REGIONS)
  val regions = TimeManager.regions
  var spotRequestInstanceId: String = _
  var instanceId: String = _
  var region: Regions = _
  val accessKeyId = ConfigFactory.load()
    .getString(s"${InstanceRegionFactory.AWS}.${InstanceRegionFactory.ACCESS_KEY_ID}")
  val secretKeyId = ConfigFactory.load()
    .getString(s"${InstanceRegionFactory.AWS}.${InstanceRegionFactory.SECRET_KEY_ID}")

  def init() = {

    val delayMilisecondsForCreation = ThreadLocalRandom.current().nextInt(0, maxMiliseconds)
    context.system.scheduler.scheduleOnce(Duration.create(delayMilisecondsForCreation, TimeUnit.MILLISECONDS),
      self, CreateInstance())

    S3Logger.scheduledNode(self.path.name)
    log.info(s"::::::Init: Node ${self.path.name} will be created in $delayMilisecondsForCreation miliseconds")
  }


  def createInstance() = {


    val cal = Calendar.getInstance()
    cal.setTime(new Date())
    val hour = cal.get(Calendar.HOUR_OF_DAY)

    val regionRange = regions.map(region => regionsConf.getInt(s"$hour.$region"))
      .scan(regionsConf.getInt(s"$hour.${regions.head}"))(_ + _).zip(regions)

    val randomForRegion = ThreadLocalRandom.current().nextInt(0, regionRange.last._1)

    val selectedRegion = regionRange.find(rangeTuple => randomForRegion < rangeTuple._1).map(_._2).getOrElse(Node.US_EAST_1).toString


    regionActors.get(selectedRegion) match {
      case Some(actor) =>
        actor ! CreateNode(self)
    }


  }


  def deleteInstance() = {

    //TODO: Delete instance
    S3Logger.deletedNode(self.path.name)
    val awsCreds = new BasicAWSCredentials(accessKeyId, secretKeyId)
    val ec2 = AmazonEC2ClientBuilder.standard()
      .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
      .withRegion(region)
      .build()
    val cancelRequest =
      new CancelSpotInstanceRequestsRequest(Seq(spotRequestInstanceId).asJava)
    ec2.cancelSpotInstanceRequests(cancelRequest)
    val terminateRequest = new TerminateInstancesRequest(Seq(instanceId).asJava)
    ec2.terminateInstances(terminateRequest)


    context.stop(self)


  }

  def successfulCreation(iId: String, sRIId: String, r: Regions) = {
    val ttl = ThreadLocalRandom.current().nextInt(minTTLMinutes, maxTTLMinutes)
    instanceId = iId
    spotRequestInstanceId = sRIId
    region = r
    context.parent ! SetActiveNode(self.path.name)
    context.system.scheduler.scheduleOnce(Duration.create(ttl, TimeUnit.MILLISECONDS),
      self, DeleteInstance())
    S3Logger.createdNode(self.path.name, region.getName, "", instanceId, spotRequestInstanceId)
    log.info(s"::::::CreateInstance: Node ${self.path.name} has been created and will be deleted in $ttl miliseconds")
  }

  override def receive: Receive = {
    case Init() => init()
    case CreateInstance() => createInstance()
    case DeleteInstance() => deleteInstance()
    case SuccessfulCreation(instanceId, spotRequestInstanceId, region) =>
      successfulCreation(instanceId, spotRequestInstanceId, region)


  }
}
