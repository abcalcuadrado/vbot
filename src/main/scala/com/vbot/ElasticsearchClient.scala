package com.vbot

import java.net.{Inet4Address, InetAddress}

import com.sksamuel.elastic4s.{ElasticClient, ElasticsearchClientUri}
import org.elasticsearch.common.settings.Settings
import com.typesafe.config.ConfigFactory
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.xpack.client.PreBuiltXPackTransportClient

/**
  * Created by vbot on 1/15/17.
  */
object ElasticsearchClient {

  val ELASTICSEARCH = "elasticsearch"
  val HOST = "host"
  val PORT = "port"
  val CLUSTER_NAME = "clusterName"
  val esHost = ConfigFactory.load().getString(s"$ELASTICSEARCH.$HOST")
  val esPort = ConfigFactory.load().getString(s"$ELASTICSEARCH.$PORT")
  val clusterName = ConfigFactory.load().getString(s"$ELASTICSEARCH.$CLUSTER_NAME")

  val client = {
    val settings = Settings.builder()
      .put("client.transport.nodes_sampler_interval", "5s")
      .put("client.transport.sniff", false)
      .put("transport.tcp.compress", true)
      .put("cluster.name", clusterName)
      .put("xpack.security.transport.ssl.enabled", true)
      .put("request.headers.X-Found-Cluster", "${cluster.name}")
      .put("xpack.security.user", "elastic:MaN7wEjXFT05aNN2kgqoV4Z4")
      .build();

    val client = new PreBuiltXPackTransportClient(settings)
    InetAddress.getAllByName(esHost).find(address => address.isInstanceOf[Inet4Address]) match {
      case Some(address) => client.addTransportAddress(new InetSocketTransportAddress(address, 9343));
    }


    ElasticClient.fromClient(client)

  }
}
