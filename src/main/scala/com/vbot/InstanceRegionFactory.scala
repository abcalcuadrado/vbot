package com.vbot

import java.util
import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef}
import akka.actor.Actor.Receive
import akka.event.Logging
import com.amazonaws.auth.{AWSStaticCredentialsProvider, BasicAWSCredentials}
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.regions.Regions
import com.amazonaws.services.ec2.{AmazonEC2Client, AmazonEC2ClientBuilder}
import com.amazonaws.services.ec2.model.{CancelSpotInstanceRequestsRequest, DescribeSpotInstanceRequestsRequest, LaunchSpecification, RequestSpotInstancesRequest}
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.typesafe.config.ConfigFactory
import com.vbot.InstanceRegionFactoryProtocol.{CreateNode, ResetPrice}
import com.vbot.NodeProtocol.{CreateInstance, SuccessfulCreation}

import scala.concurrent.ExecutionContext.Implicits.global
import collection.JavaConverters._
import scala.concurrent.duration.Duration
import scala.util.Success

/**
  * Created by vbot on 1/18/17.
  */

object InstanceRegionFactoryProtocol {

  case class ResetPrice()

  case class CreateNode(nodeSender: ActorRef)



}

object InstanceRegionFactory {
  val INSTANCE_PRICES = "instancePrices"
  val INSTANCE_SETTINGS = "instanceSettings"
  val INSTANCE_TYPE = "instanceType"
  val MIN = "min"
  val MAX = "max"
  val AMI = "ami"
  val RESET_FACTOR = "resetFactor"
  val INCREASE_FACTOR = "increaseFactor"
  val SECURITY_GROUP_NAME = "securityGroupName"
  val PRICE_TO_LOW = "price-too-low"
  val PENDING_EVALUATION = "pending-evaluation"
  val PENDING_FULFILLMENT = "pending-fulfillment"
  val FULFILLED = "fulfilled"
  val OPEN = "open"
  val INSTANCE_CREATION_LOG_TYPE = "instance_creation"
  val INSTANCE_DELETION_LOG_TYPE = "instance_deletion"
  val ACCESS_KEY_ID = "accessKeyId"
  val SECRET_KEY_ID = "secretKeyId"
  val AWS = "aws"
  val TIME_TO_RESET = "timeToReset"
}

class InstanceRegionFactory(region: Regions) extends Actor {


  var currentPrice: Double = _
  val min = ConfigFactory.load().getDouble(s"${InstanceRegionFactory.INSTANCE_PRICES}.${InstanceRegionFactory.MIN}")
  val max = ConfigFactory.load().getDouble(s"${InstanceRegionFactory.INSTANCE_PRICES}.${InstanceRegionFactory.MAX}")
  val resetFactor = ConfigFactory.load()
    .getDouble(s"${InstanceRegionFactory.INSTANCE_PRICES}.${InstanceRegionFactory.RESET_FACTOR}")
  val increaseFactor = ConfigFactory.load()
    .getDouble(s"${InstanceRegionFactory.INSTANCE_PRICES}.${InstanceRegionFactory.INCREASE_FACTOR}")
  val ami = ConfigFactory.load()
    .getString(s"${InstanceRegionFactory.INSTANCE_SETTINGS}.${region.getName}.${InstanceRegionFactory.AMI}")
  val instanceType = ConfigFactory.load()
    .getString(s"${InstanceRegionFactory.INSTANCE_SETTINGS}.${region.getName}.${InstanceRegionFactory.INSTANCE_TYPE}")
  val securityGroupName = ConfigFactory.load()
    .getString(s"${InstanceRegionFactory.INSTANCE_SETTINGS}.${region.getName}.${InstanceRegionFactory.SECURITY_GROUP_NAME}")
  val accessKeyId = ConfigFactory.load()
    .getString(s"${InstanceRegionFactory.AWS}.${InstanceRegionFactory.ACCESS_KEY_ID}")
  val secretKeyId = ConfigFactory.load()
    .getString(s"${InstanceRegionFactory.AWS}.${InstanceRegionFactory.SECRET_KEY_ID}")
  val timeToReset = ConfigFactory.load()
    .getInt(s"${InstanceRegionFactory.INSTANCE_PRICES}.${InstanceRegionFactory.TIME_TO_RESET}")
  val log = Logging(context.system, this)
  var spotRequestInstanceId: String = _
  var instanceId: String = _
  context.system.scheduler.schedule(Duration.create(20, TimeUnit.MINUTES), Duration.create(20, TimeUnit.MINUTES), self, ResetPrice())

  override def preStart(): Unit = {
    currentPrice = min
    super.preStart()
  }

  def createNode(nodeSender: ActorRef) = {

    var created = false
    do {

      val awsCreds = new BasicAWSCredentials(accessKeyId, secretKeyId)
      val ec2 = AmazonEC2ClientBuilder.standard()
        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
        .withRegion(region)
        .build()

      val requestRequest = new RequestSpotInstancesRequest()
      requestRequest.setSpotPrice(currentPrice.toString)
      requestRequest.setInstanceCount(Integer.valueOf(1))
      val launchSpecification = new LaunchSpecification()
      launchSpecification.setImageId(ami)
      launchSpecification.setInstanceType(instanceType)
      val securityGroups = new util.ArrayList[String]()
      securityGroups.add(securityGroupName)
      launchSpecification.setSecurityGroups(securityGroups)
      requestRequest.setLaunchSpecification(launchSpecification)

      val requestResult = ec2.requestSpotInstances(requestRequest)

      val requestResponses = requestResult.getSpotInstanceRequests
      val spotInstanceRequestIds = requestResponses.asScala
        .map(requestResponse => requestResponse.getSpotInstanceRequestId)

      var retry = false

      do {
        Thread.sleep(5000)

        val describeRequest = new DescribeSpotInstanceRequestsRequest()
        describeRequest.setSpotInstanceRequestIds(spotInstanceRequestIds.asJava)
        retry = false
        try {

          val describeResult = ec2.describeSpotInstanceRequests(describeRequest)
          val describeResponses = describeResult.getSpotInstanceRequests()

          describeResponses.asScala.foreach(dR => {
            S3Logger.logToS3(InstanceRegionFactory.INSTANCE_CREATION_LOG_TYPE,
              dR.getSpotInstanceRequestId + "," + dR.getInstanceId + "," + dR.getState + ","
                + dR.getStatus.getCode + "," + dR.getStatus.getMessage + "," + region.getName + ","
                + System.currentTimeMillis() + "," + currentPrice, UUID.randomUUID().toString)
            instanceId = dR.getInstanceId
            spotRequestInstanceId = dR.getSpotInstanceRequestId
          })


          created = describeResponses.asScala.exists(describeResponse =>
            describeResponse.getStatus.getCode == InstanceRegionFactory.FULFILLED)


          if (!created) {
            retry = describeResponses.asScala.exists(describeResponse =>
              describeResponse.getStatus.getCode == InstanceRegionFactory.PENDING_EVALUATION ||
                describeResponse.getStatus.getCode == InstanceRegionFactory.PENDING_FULFILLMENT)


            val spotInstanceRequestIdsForCancellation = describeResponses.asScala.filter(describeResponse => {

              describeResponse.getStatus.getCode != InstanceRegionFactory.PENDING_FULFILLMENT &&
                describeResponse.getStatus.getCode != InstanceRegionFactory.PENDING_EVALUATION &&
                describeResponse.getStatus.getCode != InstanceRegionFactory.FULFILLED

            }).map(describeResponse => {
              if (describeResponse.getStatus.getCode == InstanceRegionFactory.PRICE_TO_LOW && currentPrice < max)
                currentPrice += increaseFactor
              describeResponse

            }).map(describeResponse => describeResponse.getSpotInstanceRequestId).asJava

            if (spotInstanceRequestIdsForCancellation.size() > 0) {
              val cancelRequest = new CancelSpotInstanceRequestsRequest(spotInstanceRequestIdsForCancellation)
              ec2.cancelSpotInstanceRequests(cancelRequest)
              created = false
            }
          }

        } catch {
          case ex: Throwable =>
            println(ex.getMessage)
            retry = true;
        }


      } while (retry)


    } while (!created)
    nodeSender ! SuccessfulCreation(instanceId, spotRequestInstanceId, region)

  }

  def resetPrice = {
    if ((currentPrice - resetFactor) < min) currentPrice = min
    else currentPrice -= resetFactor
  }

  override def receive: Receive = {
    case ResetPrice() => resetPrice
    case CreateNode(nodeSender) => createNode(nodeSender)

  }


}
