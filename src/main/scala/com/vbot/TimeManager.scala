package com.vbot

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.{Calendar, Date, UUID}
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, Props, Terminated}
import akka.actor.Actor.Receive
import akka.event.Logging
import com.amazonaws.regions.Regions
import com.typesafe.config.ConfigFactory
import com.vbot.TimeManagerProtocol._

import scala.collection.immutable.ListMap
import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by vbot on 1/13/17.
  */

object TimeManagerProtocol {

  case class Init()

  case class Tick()

  case class SetActiveNode(name: String)


}

object TimeManager {
  val TIME_MANAGER = "timeManager"
  val TIME_RANGE_MILISECONDS = "timeRangeMiliseconds"
  val SCHEDULES = "schedules"
  val TOTAL_HOURS = "totalHours"
  val MONDAY = "mon"
  val TUESDAY = "tue"
  val WEDNESDAY = "wed"
  val THURSDAY = "thu"
  val FRIDAY = "fri"
  val SATURDAY = "sat"
  val SUNDAY = "sun"
  val regions = Seq(Node.US_EAST_1, Node.US_EAST_2, Node.US_WEST_1, Node.US_WEST_2, Node.CA_CENTRAL_1, Node.EU_CENTRAL_1,
    Node.EU_WEST_1, Node.EU_WEST_2, Node.SA_EAST_1, Node.AP_SOUTHEAST_2)
}

class TimeManager extends Actor {
  val log = Logging(context.system, this)
  val conf = ConfigFactory.load()
  var notActiveNodes: ListMap[String, ActorRef] = ListMap()
  var activeNodes: ListMap[String, ActorRef] = ListMap()
  val regionActors: Map[String, ActorRef] = TimeManager.regions.map(region =>
    region -> context.actorOf(Props(classOf[InstanceRegionFactory], Regions.fromName(region)), region)).toMap


  val percentageDistribution = {
    val daysOfWeek = List(TimeManager.MONDAY, TimeManager.TUESDAY, TimeManager.WEDNESDAY, TimeManager.THURSDAY,
      TimeManager.FRIDAY, TimeManager.SATURDAY, TimeManager.SUNDAY)
    ListMap(daysOfWeek.map(day => (day, _getPercentagePerDay(day))): _*)

  }

  val totalHours = conf.getInt(s"${TimeManager.TIME_MANAGER}.${TimeManager.TOTAL_HOURS}")

  def _getPercentagePerDay(day: String): Int = {
    val schedulesConf = conf.getConfig(TimeManager.SCHEDULES)
    (0 to 23).fold(0)((total: Int, hour: Int) => {
      schedulesConf.getInt(s"$hour.$day") + total
    })
  }

  def _getDayOfWeekAdding(addedDays: Int) = LocalDate.now().plusDays(addedDays).format(DateTimeFormatter.ofPattern("E")).toLowerCase()

  def _getTotalPerMonthOnPercentage = (0 to 29).fold(0)((total: Int, dayOfMonth) => {
    percentageDistribution.getOrElse(_getDayOfWeekAdding(dayOfMonth), 0) + total
  })

  def _getActualHourPercentage = {
    val cal = Calendar.getInstance()
    cal.setTime(new Date())
    val hour = cal.get(Calendar.HOUR_OF_DAY)
    val day = _getDayOfWeekAdding(0)
    conf.getInt(s"${TimeManager.SCHEDULES}.$hour.$day")
  }

  def init() = {
    val range = conf.getConfig(TimeManager.TIME_MANAGER).getInt(TimeManager.TIME_RANGE_MILISECONDS)
    context.system.scheduler.schedule(Duration.create(0, TimeUnit.MILLISECONDS), Duration.create(range, TimeUnit.MILLISECONDS), self, Tick())

  }

  def tick() = {

    val nodes = _getActualHourPercentage * totalHours / _getTotalPerMonthOnPercentage

    val missingNodes = nodes - (notActiveNodes.size + activeNodes.size)
    log.info(s"total nodes $missingNodes ${notActiveNodes.size + activeNodes.size}")

    if (missingNodes > 0) {

      notActiveNodes = ListMap[String, ActorRef]((1 to missingNodes)
        .map(_ => UUID.randomUUID().toString)
        .map(uuid => {
          val nodeActor = context.actorOf(Props(classOf[Node], regionActors), uuid)
          context.watch(nodeActor)
          nodeActor ! NodeProtocol.Init()
          (uuid, nodeActor)
        }): _*) ++ notActiveNodes

    }


  }

  def _getSchedules(): ListMap[Int, Double] = {
    val hours = 0 to 23

    val schedules = conf.getConfig("schedules")
    ListMap(hours.map(hour => (hour, schedules.getDouble(hour.toString))): _*)
  }

  def setActiveNode(actorName: String) = {

    notActiveNodes.find(nameActorRefTuple => nameActorRefTuple._1 == actorName) match {
      case Some(nameActorRefTuple) => activeNodes = activeNodes + nameActorRefTuple
      case None =>
    }
    notActiveNodes = notActiveNodes.filter(nameActorRefTuple => nameActorRefTuple._1 != actorName)
  }


  def deleteNode(node: ActorRef) = {
    context.unwatch(node)
    notActiveNodes = notActiveNodes.filter(nameActorRefTuple => nameActorRefTuple._1 != node.path.name)
    activeNodes = activeNodes.filter(nameActorRefTuple => nameActorRefTuple._1 != node.path.name)
    log.info(s"::::::Terminated: Node ${node.path.name} has been detached of TimeManager")
  }

  override def receive: Receive = {
    case Init() => init()
    case Tick() => tick()
    case SetActiveNode(actorName) => setActiveNode(actorName)
    case Terminated(nodeActorRef) => deleteNode(nodeActorRef)
  }
}
