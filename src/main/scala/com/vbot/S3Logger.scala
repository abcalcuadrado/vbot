package com.vbot

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, OutputStreamWriter}
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Date

import com.amazonaws.auth.{AWSStaticCredentialsProvider, BasicAWSCredentials}
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.{ObjectMetadata, PutObjectRequest}
import com.google.gson.Gson
import com.typesafe.config.ConfigFactory

/**
  * Created by vbot on 1/15/17.
  */

case class NodeActivity(id: String, region: String, availabilityZone: String, scheduledAt: String,
                        createdAt: String, deletedAt: String, status: String, instanceId: String,
                        spotRequestInstanceId: String)

object S3Logger {

  val SCHEDULED = "SCHEDULED"
  val CREATED = "CREATED"
  val DELETED = "DELETED"
  val NODE_ACTIVITY = "node_activity"
  val S3 = "s3"
  val BUCKET = "bucket"
  val bucket = ConfigFactory.load().getString(s"$S3.$BUCKET")
  val ACCESS_KEY_ID = "accessKeyId"
  val SECRET_KEY_ID = "secretKeyId"
  val AWS = "aws"
  val accessKeyId = ConfigFactory.load()
    .getString(s"$AWS.$ACCESS_KEY_ID")
  val secretKeyId = ConfigFactory.load()
    .getString(s"$AWS.$SECRET_KEY_ID")

  def logToS3(logType: String, message: String, name: String): Unit = {
    //    val json = s"${nodeActivity.id},${nodeActivity.region},${nodeActivity.availabilityZone},${nodeActivity.status}," +
    //      s"${nodeActivity.scheduledAt},${nodeActivity.createdAt},${nodeActivity.deletedAt}"
    val baos = new ByteArrayOutputStream()
    val writer = new OutputStreamWriter(baos)
    writer.write(message)
    writer.flush()
    writer.close()

    val awsCreds = new BasicAWSCredentials(accessKeyId, secretKeyId)
    val s3Client = AmazonS3ClientBuilder.standard()
      .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
      .withRegion(Regions.US_EAST_1)
      .build()

    //
    //    val key = s"$bucket/day=${LocalDate.now().format(DateTimeFormatter.ofPattern("yy-MM-dd"))}/" +
    //      nodeActivity.id + "_" + nodeActivity.status + ".json"
    val key = s"$logType/day=${LocalDate.now().format(DateTimeFormatter.ofPattern("yy-MM-dd"))}/" + name
    val metadata = new ObjectMetadata()
    metadata.setContentLength(baos.toByteArray.length)
    s3Client.putObject(new PutObjectRequest(bucket, key,
      new ByteArrayInputStream(baos.toByteArray), new ObjectMetadata()))
  }


  def scheduledNode(id: String): Unit = {
    val nodeActivity = NodeActivity(id, "", "", System.currentTimeMillis.toString, 0.toString, 0.toString, SCHEDULED, "", "")
    logToS3("NodeActivity", getMessageFromNodeActivity(nodeActivity), getNameFromNodeActivity(nodeActivity))

  }

  private def getNameFromNodeActivity(nodeActivity: NodeActivity) = {
    nodeActivity.id + "_" + nodeActivity.status + ".json"
  }

  private def getMessageFromNodeActivity(nodeActivity: NodeActivity) = {
    val json = s"${nodeActivity.id},${nodeActivity.region},${nodeActivity.availabilityZone},${nodeActivity.status}," +
      s"${nodeActivity.instanceId},${nodeActivity.spotRequestInstanceId}," +
      s"${nodeActivity.scheduledAt},${nodeActivity.createdAt},${nodeActivity.deletedAt}"
    json
  }

  def createdNode(id: String, region: String, availabilityZone: String, instanceId: String,
                  spotRequestInstanceId: String): Unit = {
    val nodeActivity = NodeActivity(id, region, availabilityZone, 0.toString, System.currentTimeMillis.toString,
      0.toString, CREATED, instanceId, spotRequestInstanceId)
    logToS3("NodeActivity", getMessageFromNodeActivity(nodeActivity), getNameFromNodeActivity(nodeActivity))

  }

  def deletedNode(id: String): Unit = {
    val nodeActivity = NodeActivity(id, "", "", 0.toString, 0.toString, System.currentTimeMillis.toString, DELETED,
      "", "")
    logToS3("NodeActivity", getMessageFromNodeActivity(nodeActivity), getNameFromNodeActivity(nodeActivity))

  }

}
