package com.vbot

import akka.actor.{ActorSystem, Props}
import com.vbot.TimeManagerProtocol.Init

/**
  * Created by vbot on 1/13/17.
  */
object Main extends App {

  val system = ActorSystem("vbot")
  system.actorOf(Props[TimeManager]) ! Init()


}
