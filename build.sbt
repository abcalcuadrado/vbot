name := "vbot"

version := "1.0"

scalaVersion := "2.12.1"

resolvers += "Maven Central Server" at "http://repo1.maven.org/maven2"


libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.4.16"
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.4.16"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
libraryDependencies += "com.typesafe" % "config" % "1.3.1"
libraryDependencies += "com.sksamuel.elastic4s" %% "elastic4s-core" % "5.1.5"
libraryDependencies += "com.sksamuel.elastic4s" %% "elastic4s-xpack-security" % "5.1.5"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.7"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"
libraryDependencies += "org.elasticsearch.plugin" % "shield" % "2.4.4"
libraryDependencies += "com.google.code.gson" % "gson" % "2.8.0"
libraryDependencies += "com.amazonaws" % "aws-java-sdk" % "1.11.78"


mainClass := Some("com.vbot.Main")
